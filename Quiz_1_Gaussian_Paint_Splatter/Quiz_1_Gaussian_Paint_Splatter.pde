void setup()
{
  size(1920, 1080, P3D);
  camera(0,0, -(height / 2.0) / tan(PI * 30 / 180),
    0,0,0,
    0, -1, 0);
  background(0);
    
}

void draw()
{
    // reset
  if(frameCount == 1000)
  {
    clear();
    frameCount = 0;
  }
  // for xpos
  float gauss = randomGaussian();
  float std1 = 300;
  float mean = 10;
  // for size
  float gauss2 = randomGaussian();
  float std2 = 50;
  float mean2 = 25;
  
  float splatterSize = (std2 * gauss2) + mean2;
  float xPos = (std1 * gauss) + mean;
  float yPos = random(-height, height);
  
  noStroke();
  fill (random(255),random(255),random(255), random(10, 100) );
  circle(xPos,yPos, splatterSize);
  
  

  
}
