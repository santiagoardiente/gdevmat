class Walker
{
 float xPosition, yPosition;
 
 void render()
 {
   circle(xPosition, yPosition,30);
 }
 
 void randomWalk()
 {
   int decision = floor(random(8)); // 0 to 3.99
   
  if (decision == 0)
  {
    yPosition += 4;
  }
  else if (decision == 1)
  {
    yPosition -= 4;
  }
  else if (decision == 2)
  {
    xPosition += 4;
  }
  else if (decision == 3)
  {
    xPosition -= 4;
  }
  else if (decision == 4)
  {
    xPosition += 4;
    yPosition += 4;
  }
  else if (decision == 5)
  {
    xPosition -= 4;
    yPosition -= 4;
  }
   else if (decision == 6)
  {
    xPosition -= 4;
    yPosition += 4;
  }
     else if (decision == 7)
  {
    xPosition += 4;
    yPosition -= 4;
  }
  fill(random(255),random(255),random(255));
  noStroke();
  
 }
}
